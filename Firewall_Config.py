# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 15:23:25 2018

@author: rajaks
"""

from openpyxl import Workbook
import argparse


parser = argparse.ArgumentParser(description="Extract text file to Excel for Firewall Config")
parser.add_argument('textFile', help="Text File to be parsed")
parser.add_argument('outFile', help="Name of outfile to be created")
    
try:
    args = parser.parse_args()
except IOError, msg:
    parser.error(str(msg))

wb = Workbook()
shtlst=[]
shtname={}


# Block to get the list of sheetnames and create seperate sheet for the VDOM
i=0
count=0
with open(args.textFile,'rt') as  txtfile:
    for line in txtfile:
        
        if "config vdom" in line:
            count=1
            continue
        elif count==1:
            if "edit " in line:
                
                tabword=line.split(" ")
                
                shtlst.append(tabword[1])
                shtname[i]=wb.create_sheet(tabword[1])
                i=i+1
                continue
            elif "config global" in line:
                count=0
                break
            else:
                continue
    
txtfile.close
#wb.save("Config_parse.xlsx")
#Block to create entries in the Root sheet. Capture the initial lines"

row=1
with open(args.textFile,'rt') as  txtfile: 
        flag=0
        for line in txtfile:
            if "config vdom" in line:
                flag=1
                break
            else:
                
                shtname[0]['A'+str(row)].value=line
                row+=1
                
                
            
txtfile.close 
#wb.save("Config_parse.xlsx")
with open(args.textFile,'rt') as  txtfile: 
        flag=0
        for line in txtfile:
            if "config global" in line:
                
                flag=1
                shtname[0]['A'+str(row)].value=line
                row+=1
            elif flag==1:
                shtname[0]['A'+str(row)].value=line
                row+=1
            elif "edit ATRI" in line:
                flag=0
            
txtfile.close   
#wb.save("Config_parse.xlsx")
             

#Iterate through Sheet list and create contents for the respective sheet

for item in range(1,len(shtlst)-1):
    count=0
    with open(args.textFile,'rt') as  txtfile:
        row=1
        flag=0
        #print shtlst[item]
        for line in txtfile:
            
            if "config global" in line:
                count=1
                continue
            elif count==1:
               if "edit "+shtlst[item] in line:
                   
                   flag=1
                   shtname[item]['A'+str(row)].value=line
                   row+=1
               elif flag==1:
                   if "edit "+shtlst[item+1] in line:
                       flag=0
                       count=0
                       break
                   else:
                       shtname[item]['A'+str(row)].value=line
                       row+=1           
               
    txtfile.close
#wb.save("Config_parse.xlsx")

#Block to create entries in the Last sheet."
if shtlst[-1]:
   
    row=1
    with open(args.textFile,'rt') as  txtfile: 
            flag=0
            for line in txtfile:
                if "config global" in line:
                    count=1
                    continue
                elif count==1:
                    if "edit "+shtlst[len(shtlst)-1] in line:       
                        flag=1
                        shtname[len(shtlst)-1]['A'+str(row)].value=line
                        row+=1
                    elif flag==1:
                        
                        shtname[len(shtlst)-1]['A'+str(row)].value=line
                        row+=1     
    txtfile.close
#wb.save("Config_parse.xlsx")    

#Remove default sheet

rem=wb["Sheet"]
wb.remove(rem)

wb.save(args.outFile)  






              
                
                