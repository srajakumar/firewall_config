# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 18:02:12 2018

@author: rajaks
"""

Pre-requisites:

    1. openpyxl
    2. argparse
    
Files required:
    1. Firewall_Config.py - Python Source code file
    2. Cluster01-FW-RawConfig.txt - The Text configuration file

How to run:
    python Firewall_Config.py <argument1> <argument2>
    
    <argument1> - Input file name which is nothing but the text configuration file
    <argument2> - Output file which has to be created.
    